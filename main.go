package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type ViewData struct{

	Text string
	Result int
}

func main() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {



		data := ViewData{
			Text: "World Cup",
			Result: 1234,
		}
		tmpl, _ := template.ParseFiles("pages/main.html")
		tmpl.Execute(w, data)
	})

	http.HandleFunc("/about", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "About this web site!")
	})

	fmt.Println("Server is listening...")

	http.ListenAndServe(":8081", nil)

}